#include "Checkpoint.h"
#include <stdio.h>


Checkpoint::Checkpoint(void)
{
}


Checkpoint::~Checkpoint(void)
{
}

void Checkpoint::loadImage(void)
{
	printf("Loaded in checkpoint successfully\n");
	image = (HBITMAP)LoadImage(0, "Sprites/checkpoint.bmp", IMAGE_BITMAP, 0 , 0, LR_LOADFROMFILE);

	GetObject(image,sizeof(BITMAP), &map);
}

void Checkpoint::draw(HDC targetHDC)
{
	if(xPosition > -32 && xPosition < 808)
	{
		HDC tempHDC;

		tempHDC = CreateCompatibleDC(targetHDC);
		SelectObject(tempHDC,image);

		TransparentBlt(targetHDC,xPosition,yPosition,map.bmWidth,	map.bmHeight,	tempHDC,	0,0,32,32,	RGB(255,0,255));


		DeleteDC(tempHDC);


		printf("The checkpoint has been successfully drawn to the screen!\n");
	}
}

void Checkpoint::cleanUp(void)
{
	printf("The checkpoint has been sucessfully deleted from the game!\n");
}
void Checkpoint::Update(void)
{
	xPosition-=8;
}
int Checkpoint::getX()
{
	return xPosition;
}
int Checkpoint::getY()
{
	return yPosition;
}
void Checkpoint::create()
{
	initialXPos = xPosition;
	initialYPos = yPosition;
}

void Checkpoint::reset()
{
	xPosition = initialXPos;
	yPosition = initialYPos;
}