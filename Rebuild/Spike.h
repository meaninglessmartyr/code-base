#pragma once
#include "GameObject.h"
class Spike : public GameObject
{
private:
	int initialXPos;
	int initialYPos;
public:
	Spike(void);
	~Spike(void);
	void reset(void);
	void create();

	int getX();
	int getY();
	void draw(HDC);
	void cleanUp(void);
	void loadImage(void);
	void update();
};
