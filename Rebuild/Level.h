#pragma once


#include "Block.h"
#include "Checkpoint.h"
#include "Spike.h"
#include "finish.h"
#include <list>
class Level
{
private:

	std::list<finish> myFinish;
	std::list<Block> myBlocks;
	std::list<Checkpoint> myCheckpoints;
	std::list<Spike> mySpikes;

	int blockCount;
	int spikeCount;
	int checkpointCount;
	bool loadLevelFlag;
	char levelArray[30][94];

public:

	int getBlockX(int);
	int getBlockY(int);
	int getSpikeX(int);
	int getSpikeY(int);
	int getCheckpointX(int);
	int getCheckpointY(int);
	int getEndX();
	int getEndY();

	void reset(void);

	int getBlockCount(void);
	int getSpikeCount(void);
	int getCheckpointCount(void);
	bool readLevel(char* filename);
	void cleanUp(void);
	void printLevel(void);
	void checkType(char, int , int);
	void init(void);
	void loadImages(void);
	void drawObjects(HDC);
	int getAtIndex(int,int);
	void update(void);
	Level(void);
	~Level(void);
};

