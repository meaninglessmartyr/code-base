#pragma once
#include "GameObject.h"
class Checkpoint : public GameObject
{
private:
	int initialXPos;
	int initialYPos;
public:
	void create(void);
	void reset(void);
	Checkpoint(void);
	~Checkpoint(void);
	int getX();
	int getY();
	void Update();
	void draw(HDC);
	void cleanUp(void);
	void loadImage(void);
};

