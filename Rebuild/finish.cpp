#include "finish.h"
#include <stdio.h>


finish::finish(void)
{
}


finish::~finish(void)
{
}

void finish::loadImage(void)
{
	printf("Loaded in finish successfully\n");
	image = (HBITMAP)LoadImage(0, "Sprites/finish.bmp", IMAGE_BITMAP, 0 , 0, LR_LOADFROMFILE);

	GetObject(image,sizeof(BITMAP), &map);
}

void finish::draw(HDC targetHDC)
{
	if(xPosition > -32 && xPosition < 808)
	{
		HDC tempHDC;

		tempHDC = CreateCompatibleDC(targetHDC);
		SelectObject(tempHDC,image);

		TransparentBlt(targetHDC,xPosition,yPosition,map.bmWidth,	map.bmHeight,	tempHDC,	0,0,32,32,	RGB(255,0,255));


		DeleteDC(tempHDC);


		printf("The finish has been successfully drawn to the screen!\n");
	}
}

void finish::cleanUp(void)
{
	printf("The finish has been sucessfully deleted from the game!\n");
}
void finish::Update(void)
{
	xPosition-=8;
}
int finish::getX()
{
	return xPosition;
}
int finish::getY()
{
	return yPosition;
}
void finish::create()
{
	initialXPos = xPosition;
	initialYPos = yPosition;
}

void finish::reset()
{
	xPosition = initialXPos;
	yPosition = initialYPos;
}