#pragma once
#include <Windows.h>

class GameObject
{
protected:

	int xPosition;
	int yPosition;
	//adding an image 
	HBITMAP image;
	BITMAP map;


public:

	GameObject(void);
	~GameObject(void);
	virtual void draw(HDC) = 0;
	virtual void cleanUp(void) = 0;

	void setFilename(char*);
	int getXPosition(void);
	int getYPosition(void);
	void setYPosition(int s);
	void setXPosition(int s);
	virtual void loadImage(void) = 0;
};

