//Includes
#include <windows.h>
#include <time.h>
#include <string.h>
#include <stdio.h>

#include "Game.h"


#define FRAME_RATE 100
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 500

//Function headers
ATOM MyRegisterClass(HINSTANCE hinstance);
int WINAPI WinMain (  HINSTANCE hInstance,  
					HINSTANCE hPrevInstance,  
					LPSTR  lpCmdLine, 
					int nCmdShow);
void InitInstance(HINSTANCE hinstance, int nCmdShow);
LRESULT CALLBACK WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);



Game* gameEng;


//time stuff
int timeLast;
int curr_time;

//Need a global hWnd so we can send messages from update calling for a repaint
HWND globalHwnd;
HDC globalHdc;




//entry point for a Windows program
int WINAPI WinMain (  HINSTANCE hInstance,  HINSTANCE hPrevInstance,  LPSTR  lpCmdLine,
                   	 	int nCmdShow)
{
    // declare variables
	MSG msg;

    // register the class in my own function that defines and registers the Windows class
	MyRegisterClass(hInstance);

    // my function to initialize application and create the window
	InitInstance (hInstance, nCmdShow);
	if (!globalHwnd) 
		return FALSE;

    //set random number seed
	srand((int)time(NULL));

	//initalise the lastTime variable
	timeLast = GetTickCount();
   
	//Setup game

	//Create game
	gameEng = new Game(globalHwnd);

	//give game window size - reqruied for initiating duck positions
	gameEng->setWindowSize(WINDOW_WIDTH,WINDOW_HEIGHT);

	//Game Init
	gameEng->init();

	//setup back buffering
	gameEng->setupBackBuffer();


	gameEng->displaySplashScreen();
	
    // main message loop
	while (TRUE) 
	{
		//At the start of your update loop you need to get the latest time in milliseconds, calling GetTickCount()
		curr_time = GetTickCount();
		
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			// test if this is a quit
			 if (msg.message == WM_QUIT) break;
          
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		
		if( (curr_time-timeLast)>FRAME_RATE)
		{
			// for additional pecision it is best to calculate exactly how 
			//much time has elapsed since the last update and then use that 
			//fractional scaler when updating your objects
			float scaler = ((curr_time-timeLast)/FRAME_RATE);

			// set the update factor of the ducks
			gameEng->setScalar(scaler); 

			//update game
			gameEng->update();

			//draws background and ducks to back buffer
			gameEng->draw(); 
			
			// update timeLast before exiting
			timeLast = curr_time;

			//swap the back and front buffers
			gameEng->swapBuffers();
		}     
		
	}

	return msg.wParam;
}



ATOM MyRegisterClass(HINSTANCE hinstance)
{

	WNDCLASS wc;

	wc.style = CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc = (WNDPROC)WinProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hinstance;
	wc.hIcon = NULL;
	wc.hCursor = LoadCursor(NULL,IDC_ARROW);
	wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "MyClass";

	return RegisterClass(&wc);
}

// creates the instance of a window
void InitInstance(HINSTANCE hinstance, int nCmdShow)
{

	
	static TCHAR szAppName[] = TEXT("MyClass");

	globalHwnd = CreateWindow(
		    szAppName,				// window class
			TEXT("Impossible Game"),// title bar
			WS_OVERLAPPEDWINDOW,	// window style
			CW_USEDEFAULT,			// x position of window
			CW_USEDEFAULT,			// y position of window
			WINDOW_WIDTH,			// width of window
			WINDOW_HEIGHT,			// height of window
			NULL,					// parent of window
			NULL,					// menu
			hinstance,				// application instance
			NULL);					// window parameters

	

	ShowWindow(globalHwnd, nCmdShow);
	UpdateWindow(globalHwnd);

	
}


LRESULT CALLBACK WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	COLORREF c;
	RECT rt;

	switch(message)
	{
		case WM_PAINT:	  
			
		break;

		case WM_CREATE:
	    break;

		case WM_KEYDOWN:
			gameEng->handleKeyPress(wParam);
		break;

		case WM_LBUTTONDOWN:
			//myGame->handleLMouseClick(lParam);
		break;

		case WM_DESTROY: PostQuitMessage(0);
			 break;
	}
	return DefWindowProc(hWnd,message,wParam,lParam);
}

