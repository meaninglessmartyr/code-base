#pragma once
#include "GameObject.h"
class finish : public GameObject
{
private:
	int initialXPos;
	int initialYPos;
public:
	void create(void);
	void reset(void);
	finish(void);
	~finish(void);
	int getX();
	int getY();
	void Update();
	void draw(HDC);
	void cleanUp(void);
	void loadImage(void);
};

