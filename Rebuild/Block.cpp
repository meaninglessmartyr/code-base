#include "Block.h"
#include <stdio.h>

Block::Block(void)
{

}


Block::~Block(void)
{
}
void Block::create(void)
{
	initialPosX = xPosition;
	initialPosY = yPosition;
}
void Block::draw(HDC targetHDC)
{
	if(xPosition > -32 && xPosition < 808)
	{
		HDC tempHDC;

		tempHDC = CreateCompatibleDC(targetHDC);
		SelectObject(tempHDC,image);

		TransparentBlt(targetHDC,xPosition,yPosition,map.bmWidth,	map.bmHeight,	tempHDC,	0,0,32,32,	RGB(255,0,255));


		DeleteDC(tempHDC);


		printf("Have succesfully drawn the block to the screen\n");
	}
}

void Block::cleanUp(void)
{
	printf("Have sucessfully deleted the block from the game!\n");
}

void Block::loadImage(void)
{
	printf("Loaded block in successfully\n");
	image = (HBITMAP)LoadImage(0, "Sprites/block.bmp", IMAGE_BITMAP, 0 , 0, LR_LOADFROMFILE);

	GetObject(image,sizeof(BITMAP), &map);
}

int Block::getX()
{
	return xPosition;
}
int Block::getY()
{
	return yPosition;
}

void Block::update()
{
	xPosition -=8;

}
void Block::reset()
{
	xPosition = initialPosX;
	yPosition = initialPosY;
}