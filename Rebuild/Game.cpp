#include "Game.h"

//#include "Level.h"


Game::Game(HWND hwnd)
{
	hWnd = hwnd;

	mainHDC = GetDC(hWnd);

	backBMP = NULL;
	oldBMP = NULL;

	// Back rectangle?
	levelNum =1;
	


}


Game::~Game(void)
{
	
}


// Private functions

void Game::init(void)
{
	
	
	// Create level
	level = new Level();

	// Create Player
	player = new Player(200,200);


	backRT.top = 0;
	backRT.left = 0;
	backRT.bottom = HEIGHT;
	backRT.right = WIDTH;

	//loadScores();


	// Load BG iMAGE

	bgImage = (HBITMAP)LoadImage(0, "Sprites/background.bmp", IMAGE_BITMAP, 0 , 0, LR_LOADFROMFILE);

	GetObject(bgImage,sizeof(BITMAP), &map);


	// Load level from file
	char integer_string[15];
	sprintf_s(integer_string, "level%d.txt",levelNum);
	char url[25] = ("Levels/");
	strcat(url,integer_string);
	level->readLevel(url);
	level->loadImages();

}
	
void Game::cleanUp(void)
{
	// Needed here
	// delete level, player, game objects


	delete(player);
	delete(level);
}
	
void Game::swapBuffers(void)
{
	BitBlt(mainHDC, 0, 0, WIDTH, HEIGHT, backHDC, 0, 0, SRCCOPY); 
}

void Game::setupBackBuffer()
{
	backHDC = CreateCompatibleDC(mainHDC);
	backBMP = CreateCompatibleBitmap(mainHDC, WIDTH, HEIGHT);
	oldBMP = (HBITMAP)SelectObject(backHDC, backBMP);
}


void Game::setWindowSize(int width, int height)
{
	HEIGHT = height;
	WIDTH = width;
}


// Public functions

void Game::update(void)
{
	if(player->isAlive() == true)
	{

		// Update player
		int colCheck = collisionCheck();
		switch(colCheck)
		{
		case 1:
			{
				TileType type = WALL;
				player->Update(type);
				break;
			}
		case 2:
			{
				TileType type = SPIKE;
				player->Update(type);
				break;
			}
		case 3:
			{
				TileType type = EMPTY;
				player->Update(EMPTY);
				break;
			}
		case 4:
			{
				TileType type = SIDEWALL;
				player->Update(type);
			}
		}
		
		level->update();
		if(player->getX() < -32 || player->getY() > 400)
		{
			player->Update(SPIKE);
		}

		if(player->getX() > level->getEndX())//level complete
		{
			endLevel();
		}

	}
	else //player is dead
	{
		if(player->getLives() == 0)
		{
			gameOver();
		}
		else
		{
			
			int check = playerCheckpointCollision(player->getX(),player->getY());
			if(check != -1)//they have passed a checkpoint
			{
				player->reset(200,level->getCheckpointY(check));
				level->reset();
				while(level->getCheckpointX(check) > player->getX())
				{
					level->update(); 
				}
			}
			else
			{
				player->reset(200,200-16);
				level->reset();
			}
		}
	}
		/*


	write scores
	

	*/
}

int Game::collisionCheck(void)
{
	
	int playerX = player->getX();
	int playerY = player->getY();
		
	int blockHit = playerBlockCollision(playerX,playerY);
	int spikeHit = playerSpikeCollision	(playerX,playerY);
		
	if(blockHit != -1)//player has hit a block
	{
		if(blockHit == 1)
		{
			return 1;
		}
		else
		{
			return 4;
		}
	}
	else
	{
		if (spikeHit != -1)
		{
			return 2;
		}
		else
		{
			return 3;
		}
	}
		
		
		
}

int Game::playerCheckpointCollision(int x,int y)
{
	int checkX;
	int respawnX =0;
	int respawnY =0;
	int checkY;
	int checkPointID = -1;
	int count =level->getCheckpointCount();
	for (int i=0;i <count;i++)
	{
		checkX = level->getCheckpointX(i);
		checkY = level->getCheckpointY(i);
		if( checkX < x) // player has passed this
		{
			respawnX = checkX;
			respawnY = checkY;
			checkPointID = i;
			
		}
	}
	return checkPointID;
}

int Game::playerSpikeCollision(int playerX, int playerY)
{ 
		
	int spikeX;
	int spikeY;
	int dX;
	int dY;
	int closestX = NULL;
	int closestY = NULL;
	int spikeID;
	int count = level->getSpikeCount(); 
	for(int i =0;i< count; i++)
	{
		spikeX = level->getSpikeX(i);
		spikeY = level->getSpikeY(i);
		dX =  abs(spikeX - player->getX());
		dY =   abs(spikeY-16 - player->getY());
		if(dX < 14)
		{
			if(dY <14)
			{
				closestX = spikeX;
				closestY = spikeY;
				spikeID = i;	
				return spikeID;
			}
		}
	}
		
	return -1;
}

int Game::playerBlockCollision(int playerX,int playerY)
{
	int boxX;
	int boxY;
	int dX;
	int dY;
	int count = level->getBlockCount();

	for(int i =0; i < count;i++)
	{
		boxX = level->getBlockX(i);
		boxY = level->getBlockY(i);
		dX = abs(boxX-16 - playerX);
		dY = abs(boxY - playerY);
		if( dY < 10)
		{
			if(dX < 14)
			{
				return 4;
			}
		}
	}

	for(int i =0;i< count; i++)
	{
		boxX = level->getBlockX(i);
		boxY = level->getBlockY(i);
		dX =  abs(boxX - playerX);
		dY =   abs(boxY-16 - playerY);
		if(dX < 17)
		{
			if( dY <17)
			{
				
				return 1;				
			}
		}
			
	}
	

	return -1;
	
}

int Game::endLevel(void)
{
	
	// return level number - of last display scores etc
	delete(level);
	level = new Level();

	// Create Player



	// Load BG iMAGE
	levelNum++;
	if (levelNum == 4)
	{
		//no more levels
		gameOver();
	}
	else
	{
		// Load level from file
		char integer_string[15];
		sprintf_s(integer_string, "level%d.txt",levelNum);
		char url[25] = ("Levels/");
		strcat(url,integer_string);
		level->readLevel(url);
		level->loadImages();
		player->reset(200,200-16);
	}
	return 0;
	
}

bool Game::handleKeyPress(WPARAM w)
{
	
	switch(w)
	{
		case 32:
			
			player->Jump();

		break;	
	}


	return false;
}

void Game::displaySplashScreen(void)
{
	FillRect(backHDC, &backRT, (HBRUSH) GetStockObject (BLACK_BRUSH));

	int coolDown = 2000;

	HBITMAP splash1 = (HBITMAP) LoadImage(0,"Sprites/splashStory.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	HBITMAP splash2 = (HBITMAP) LoadImage(0,"Sprites/splashControl.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);;

	BITMAP s1;
	BITMAP s2;

	GetObject(splash1,sizeof(BITMAP), &s1);
	GetObject(splash2,sizeof(BITMAP), &s2);

	HDC tempHDC;

	tempHDC = CreateCompatibleDC(mainHDC);
	SelectObject(tempHDC,splash1);

	TransparentBlt(mainHDC,0,0,s1.bmWidth,s1.bmHeight,	tempHDC,	0,0,800,500,	RGB(255,0,255));


	//swapBuffers();

	int i = 0;
	while(i < coolDown)
	{
		i++;
		TransparentBlt(mainHDC,0,0,s1.bmWidth,s1.bmHeight,	tempHDC,	0,0,800,500,	RGB(255,0,255));
	}


	tempHDC = CreateCompatibleDC(mainHDC);
	SelectObject(tempHDC,splash2);

	TransparentBlt(mainHDC,0,0,s2.bmWidth,s2.bmHeight,	tempHDC,	0,0,800,500,	RGB(255,0,255));
	
	i = 0;
	while(i < coolDown)
	{
		i++;
		TransparentBlt(mainHDC,0,0,s2.bmWidth,s2.bmHeight,	tempHDC,	0,0,800,500,	RGB(255,0,255));
	}

	


	DeleteDC(tempHDC);



}

void Game::displayScore(void)
{

}


void Game::loadScores(void)
{
	char str[3];
	int score;

	FILE* scoreFile;


	//fopen("scores.txt","rb");


	//// Hard loop value coded for now 
	//int i;

	//for(i = 0; i < 3; i++)
	//{
	//	fscanf(scoreFile,str);

	//}

}

void Game::saveScores(void)
{


}
	
	
int Game::getLevelNum(void)
{


	return 1;
}

int Game::getPlayerLives(void)
{
	int lives = 2;	//player->lives();

	return lives;
}

void Game::draw(void)
{
	
	// First draw background

	//FillRect(backHDC, &backRT, (HBRUSH) GetStockObject (BLACK_BRUSH));
	HDC tempHDC;

	tempHDC = CreateCompatibleDC(backHDC);
	SelectObject(tempHDC,bgImage);

	TransparentBlt(backHDC,0,0,map.bmWidth,	map.bmHeight,	tempHDC,	0,0,800,500,	RGB(255,0,255));


	DeleteDC(tempHDC);


	// Now call to level class - get objects to draw

	player->draw(backHDC);

	level->drawObjects(backHDC);


}


void Game::setScalar(float inp)
{
	scalar = inp;
}

void Game::gameOver()
{

}