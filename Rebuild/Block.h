#pragma once
#include "GameObject.h"

class Block : public GameObject
{
private:
	int initialPosX;
	int initialPosY;

public: 
	void resetPos();
	void create(void);
	Block(void);
	~Block(void);
	int getX();
	int getY();
	void draw(HDC);
	void cleanUp(void);
	void loadImage(void);
	void update(void);
	void reset();
};

