#include "Player.h"


Player::Player(int x, int y)
{
        lives = 3;
        xPos = x;
        yPos = y -16;
        printf("A Player has been created at %d,%d \n",xPos,yPos );
        alive = true;
        printf("The player is alive\n");
        jumpTime = 0;
        jumpable = false;

		// Load in player sprites
		strcpy(filename,"Sprites/slime1.bmp");

		loadBitmap();

}
bool Player::isAlive()
{
	if(alive ==true)
	{
		return true;
	}
	else
	{
		return false;
	}

}


Player::~Player(void)
{
        printf("A Player has been destroyed\n");
}



int Player::getX()
{
        printf("Returning a xPos value of %d\n",xPos);
        return xPos;
}
int Player::getY()
{
        printf("Returning a yPos value of %d\n",yPos);
        return yPos;
}

void Player::kill()
{
        alive = false;
        printf("The Player has been killed, one life lost\n");
        lives--;
}

void Player::Jump()
{
	if(jumpable == true)
	{
        jumping = true;
        printf("The player has jumped\n");
		jumpable = false;
	}
}

void Player::Update(TileType scalar)
{
	// DEbug
	TileType type = scalar;
	if (type == SPIKE)
	{
		this->kill();
	}
	else
	{

        printf("Player is updating\n");
        if(jumping == true)
        {
                printf("Player is currently jumping, increase yPos and xPos\n");
                yPos-= 8;
                if(jumpTime == 6)
                {
                        printf("Player has finished jumping, jumping set to false\n");
						jumpTime = 0;
                        jumping = false;
                }
                jumpTime++;
        }
        else
        {
                
                printf("Player is not jumping, increase xPos and check if player can fall\n");
                if(type == EMPTY)
                {
                        printf("Player can fall, decrease yPos\n");
                        yPos+=8;
						jumpable = false;
                }
                else
                {
					if(type == WALL)
					{
						jumpable = true;
						printf("Player cannot fall at present\n");
					}
					else
					{
						jumpable = false;
						xPos-=8;
					}
                }
        }
        printf("Player is now at %d,%d\n",xPos, yPos);
	}
}


void Player::loadBitmap()
{
	image = (HBITMAP) LoadImage(0,filename,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);

	GetObject(image,sizeof(BITMAP), &map);

}


void Player::draw(HDC targetHDC)
{
	HDC tempHDC;

	tempHDC = CreateCompatibleDC(targetHDC);
	SelectObject(tempHDC,image);

	TransparentBlt(targetHDC,xPos,yPos,map.bmWidth,	map.bmHeight,	tempHDC,	0,0,32,32,	RGB(255,0,255));


	DeleteDC(tempHDC);
	//DeleteObject((HBITMAP) image);

}

void Player::reset(int x, int y)
{
	xPos =x;
	yPos =y;
	alive = true;
}
int Player::getLives()
{
	return lives;
}