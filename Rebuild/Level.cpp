#include "Level.h"
#include <stdio.h>
#include <string.h>

Level::Level(void)
{
	loadLevelFlag = false;
	blockCount = 0;
	spikeCount = 0;
	checkpointCount = 0;

}


Level::~Level(void)
{
}

void Level::init(void)
{
	/*for(int i = 0; i < blockCount; i++)
	{
		Block b;
		b.setXPosition(i+10);
		b.setYPosition(i+10);
		myBlocks.push_back(b);
	}

	for(int j = 0; j < checkpointCount; j++)
	{
		Checkpoint c;
		c.setXPosition(j+20);
		c.setYPosition(j+20);
		myCheckpoints.push_back(c);
	}

	for(int k = 0; k < spikeCount; k++)
	{
		Spike s;
		s.setXPosition(k+30);
		s.setYPosition(k+30);
		mySpikes.push_back(s);
	}*/
}

int Level::getBlockCount(void)
{
	printf("Successfully returned the block count\n");
	return blockCount;
}

int Level::getCheckpointCount(void)
{
	printf("Successfully returned the checkpoint count\n");
	return checkpointCount;
}

int Level::getSpikeCount(void)
{
	printf("Successfully returned the spike count\n");
	return spikeCount;
}

bool Level::readLevel(char* filename)
{
	//read the level in from a txt array and fill the level array with the contents
	FILE* f = fopen(filename,"r");

	char myChar;

	int i = 0;
	int j = 0;

	while((myChar = fgetc(f)) != EOF)
	{
		if(myChar == '\n')
		{
			i++;
			j = 0;
		}

		else
		{
			levelArray[i][j] = myChar;
			j++;
		}

		checkType(myChar, i, j);
	}

	printf("Successfully read in the entire level file\n");
	loadLevelFlag = true;
	fclose(f);
	return loadLevelFlag;
}

void Level::checkType(char c, int i, int j)
{
	if(c == 'b')
	{
		Block b;
		b.setXPosition(j * 32);		
		b.setYPosition(i * 32);
		b.create();
		myBlocks.push_back(b);

		blockCount++;
		
	}

	if(c == 's')
	{

		Spike s;
		s.setXPosition(j * 32);
		s.setYPosition(i* 32);
		s.create();
		mySpikes.push_back(s);

		spikeCount++;
	}

	if(c == 'c')
	{
		Checkpoint c;
		c.setXPosition(j * 32);
		c.setYPosition(i * 32);
		c.create();
		myCheckpoints.push_back(c);


		checkpointCount++;
	}
	if(c == 'f')
	{
		finish f;
		f.setXPosition(j * 32);
		f.setYPosition(i * 32);
		f.create();
		myFinish.push_back(f);

	}
}

void Level::printLevel(void)
{
	for(int i = 0; i < 10; i++)
	{
		for(int j = 0; j < 94; j++)
		{
			printf("%c",levelArray[i][j]);
		}

		printf("\n");
	}

	printf("\n");
}

void Level::drawObjects(HDC hdc)
{
	std::list<Spike>::iterator spikeIt;

	spikeIt = mySpikes.begin();

	while(spikeIt != mySpikes.end())
	{
		spikeIt->draw(hdc);
		spikeIt++;
	}

	std::list<finish>::iterator finishIt;

	finishIt = myFinish.begin();

	while(finishIt != myFinish.end())
	{
		finishIt->draw(hdc);
		finishIt++;
	}

	std::list<Block>::iterator blockIt;

	blockIt = myBlocks.begin();

	while(blockIt != myBlocks.end())
	{
		blockIt->draw(hdc);
		blockIt++;
	}

	std::list<Checkpoint>::iterator checkpointIt;

	checkpointIt = myCheckpoints.begin();

	while(checkpointIt != myCheckpoints.end())
	{
		checkpointIt->draw(hdc);
		checkpointIt++;
	}	
}

void Level::loadImages(void)
{
	std::list<Spike>::iterator spikeIt;

	spikeIt = mySpikes.begin();

	while(spikeIt != mySpikes.end())
	{
		spikeIt->loadImage();
		spikeIt++;
	}
	std::list<finish>::iterator finishIt;

	finishIt = myFinish.begin();

	while(finishIt != myFinish.end())
	{
		finishIt->loadImage();
		finishIt++;
	}

	std::list<Block>::iterator blockIt;

	blockIt = myBlocks.begin();

	while(blockIt != myBlocks.end())
	{
		blockIt->loadImage();
		blockIt++;
	}

	std::list<Checkpoint>::iterator checkpointIt;

	checkpointIt = myCheckpoints.begin();

	while(checkpointIt != myCheckpoints.end())
	{
		checkpointIt->loadImage();
		checkpointIt++;
	}
}

void Level::cleanUp(void)
{
	blockCount = 0;
	spikeCount = 0;
	checkpointCount = 0;
	
	for(int i = 0; i < 30; i++)
	{
		for(int j = 0; j < 30; j++)
		{
			levelArray[i][j] = 'x';
		}
	}

	//for(int i = 0; i < blockCount; i++)
	//{
	//	myBlocks[i].cleanUp();
	//}

	//for(int j = 0; j < checkpointCount; j++)
	//{
	//	myCheckpoints[j].cleanUp();
	//}

	//for(int k = 0; k < spikeCount; k++)
	//{
	//	mySpikes[k].cleanUp();
	//}

	//free memory of the gameobject arrays
	/*free(myBlocks);
	free(myCheckpoints);
	free(mySpikes);*/

	myCheckpoints.clear();
	myBlocks.clear();
	mySpikes.clear();

	printf("Level has been successfully cleaned up!\n");
}

int Level::getAtIndex(int x,int y)
{
	return levelArray[x][y];
}

int Level::getBlockX(int i)
{
	std::list<Block>::iterator blockIt;
	blockIt = myBlocks.begin();

	for(int j =0; j < i;j ++)
	{
	
		blockIt++;
	}

	return blockIt->getX();
}

int Level::getBlockY(int i)
{
	std::list<Block>::iterator blockIt;
	blockIt = myBlocks.begin();

	for(int j =0; j < i;j ++)
	{
	
		blockIt++;
	}

	return blockIt->getY();
}

int Level::getSpikeX(int i)
{
	std::list<Spike>::iterator spikeIt;
	spikeIt = mySpikes.begin();

	for(int j =0; j < i;j ++)
	{
	
		spikeIt++;
	}

	return spikeIt->getX();
}

int Level::getSpikeY(int i)
{
	std::list<Spike>::iterator spikeIt;
	spikeIt = mySpikes.begin();

	for(int j =0; j < i;j ++)
	{
	
		spikeIt++;
	}

	return spikeIt->getY();
}
int Level::getCheckpointX(int i)
{
	std::list<Checkpoint>::iterator checkpointIt;

	checkpointIt = myCheckpoints.begin();

	for( int j=0; j<i;j++)
	{
		
		checkpointIt++;
	}
	return checkpointIt->getX();
}
int Level::getCheckpointY(int i)
{
	std::list<Checkpoint>::iterator checkpointIt;

	checkpointIt = myCheckpoints.begin();

	for( int j=0; j<i;j++)
	{
		
		checkpointIt++;
	}
	return checkpointIt->getY();
}

int Level::getEndX()
{
	std::list<finish>::iterator finishIt;

	finishIt = myFinish.begin();
	return finishIt->getX();
}



void Level::update()
{
	std::list<Spike>::iterator spikeIt;

	spikeIt = mySpikes.begin();

	while(spikeIt != mySpikes.end())
	{
		spikeIt->update();
		spikeIt++;
	}

	std::list<Block>::iterator blockIt;

	blockIt = myBlocks.begin();

	while(blockIt != myBlocks.end())
	{
		blockIt->update();
		blockIt++;
	}
			std::list<finish>::iterator finishIt;

	finishIt = myFinish.begin();

	while(finishIt != myFinish.end())
	{
		finishIt->Update();
		finishIt++;
	}
	
	std::list<Checkpoint>::iterator checkpointIt;

	checkpointIt = myCheckpoints.begin();

	while(checkpointIt != myCheckpoints.end())
	{
		checkpointIt->Update();
		checkpointIt++;
	}

}

void Level::reset()
{
	std::list<Block>::iterator blockIt;

	blockIt = myBlocks.begin();

	while(blockIt != myBlocks.end())
	{
		blockIt->reset();
		blockIt++;
	}

	
		std::list<Spike>::iterator spikeIt;

	spikeIt = mySpikes.begin();

	while(spikeIt != mySpikes.end())
	{
		spikeIt->reset();
		spikeIt++;
	}
	std::list<finish>::iterator finishIt;

	finishIt = myFinish.begin();

	while(finishIt != myFinish.end())
	{
		finishIt->reset();
		finishIt++;
	}
	
	std::list<Checkpoint>::iterator checkpointIt;

	checkpointIt = myCheckpoints.begin();

	while(checkpointIt != myCheckpoints.end())
	{
		checkpointIt->reset();
		checkpointIt++;
	}
}