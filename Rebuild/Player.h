#pragma once
enum TileType {WALL, SPIKE, EMPTY, SIDEWALL};
#include <stdlib.h>
#include <stdio.h>

#include <Windows.h>

class Player
{
private:
	// Added

	HBITMAP image;
	BITMAP map;

	char filename[24];

	//-------

		bool jumpable;
        bool alive;
        bool jumping;
        int jumpTime;
        int xPos;
        int yPos;
        int lives;
public:
	int getLives();
	void reset(int,int);
		bool isAlive();
        Player(int, int);
        ~Player(void);
        void Jump();
        void Update(TileType scalar);
        int getX();
        int getY();
        void kill();
		bool isJumping();


		// Added

		void loadBitmap();

		// 
		void draw(HDC);
};