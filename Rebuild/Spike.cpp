#include "Spike.h"
#include <stdio.h>


Spike::Spike(void)
{
}


Spike::~Spike(void)
{
}

void Spike::draw(HDC targetHDC)
{
	if(xPosition > -32 && xPosition < 808)
	{
		HDC tempHDC;

		tempHDC = CreateCompatibleDC(targetHDC);
		SelectObject(tempHDC,image);

		TransparentBlt(targetHDC,xPosition,yPosition,map.bmWidth,	map.bmHeight,	tempHDC,	0,0,32,32,	RGB(255,0,255));


		DeleteDC(tempHDC);


		printf("drawn Spike to screen");
	}
}

void Spike::cleanUp(void)
{
	//delete the individual class instance of spike
	printf("clean up"); 
}

void Spike::loadImage(void)
{
	printf("Loaded in spike successfully\n");
	image = (HBITMAP)LoadImage(0, "Sprites/spike.bmp", IMAGE_BITMAP, 0 , 0, LR_LOADFROMFILE);

	GetObject(image,sizeof(BITMAP), &map);
}

void Spike::update()
{
	xPosition -=8;

}

int Spike::getX()
{
	return xPosition;

}
int Spike::getY()
{
	return yPosition;
}

void Spike::create(void)
{
	initialXPos = xPosition;
	initialYPos = yPosition;
}

void Spike::reset(void)
{
	xPosition = initialXPos;
	yPosition = initialYPos;
}