#pragma once

#include <Windows.h>

#include "Player.h"
#include "Level.h"

#include <stdio.h>

class Game
{
private:

	//-----------------------------------------------------------------------------
	//	Additional to documentation
	


	int windowWidth;
	int windowHeight;




	int WIDTH;
	int HEIGHT;


	HBITMAP bgImage;
	BITMAP map;

	//-----------------------------------------------------------------------------

	




	HWND hWnd;

	HDC mainHDC;
	HDC backHDC;

	
	//collision detection
	RECT scoreRT;
	RECT playerRT;
	RECT gameObjRT;

	RECT backRT;
	HBITMAP backBMP;
	HBITMAP oldBMP;

	//-------------------------------------------------------------------------------
	// Change to documentation required maybe
	//
	// back rectangle for double buffering?
	//-------------------------------------------------------------------------------

	int score;
	bool levelCompleteFlag;
	int scrollSpeed;
	int scrollOffset;

	float scalar;
	char* filename;

	int levelFlag;

	bool hitFlag;
	int levelNum;
 
	// Instances / ptrs - To implement from other stubs

	Player* player;
	Level* level;
	//Audio* audio;


	// Private functions

	
	void cleanUp(void);
	
	


public:
	
	Game(void);
	Game(HWND hwnd);
	~Game(void);
	void update(void);
	int collisionCheck(void);

	int playerCheckpointCollision(int,int);
	int playerSpikeCollision(int,int);
	int playerBlockCollision(int,int);
	
	int endLevel(void);

	bool handleKeyPress(WPARAM w);
	void displaySplashScreen(void);
	void displayScore(void);
	void loadScores(void);
	void saveScores(void);
	
	
	int getLevelNum(void);
	int getPlayerLives(void);

	void draw(void);
	void gameOver(void);


	// Note have had to be made public functions for main
	void init(void);
	void swapBuffers(void);
	void setupBackBuffer();

	// Addtional

	void setWindowSize(int, int);
	void setScalar(float);

};

