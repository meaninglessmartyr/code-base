#pragma once
class Level
{
private:

	int blockCount;
	int spikeCount;
	int checkpointCount;
	bool loadLevelFlag;
	char levelArray[30][30];

public:
	
	int getBlockCount(void);
	int getSpikeCount(void);
	int getCheckpointCount(void);
	bool readLevel(char* filename);
	void cleanUp(void);
	void printLevel(void);
	void checkType(char);

	Level(void);
	~Level(void);
};

