#pragma once
enum TileType {WALL, SPIKE, EMPTY};
#include <stdlib.h>
#include <stdio.h>
class Player
{
private:
	bool alive;
	bool jumping;
	int jumpTime;
	int xPos;
	int yPos;
	int lives;
public:
	Player(int, int);
	~Player(void);
	void Jump();
	void Update(TileType);
	int getX();
	int getY();
	void kill();
};

