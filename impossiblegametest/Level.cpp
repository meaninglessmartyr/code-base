#include "Level.h"
#include <stdio.h>
#include <string.h>

Level::Level(void)
{
	loadLevelFlag = false;
	blockCount = 0;
	spikeCount = 0;
	checkpointCount = 0;
}


Level::~Level(void)
{
}

int Level::getBlockCount(void)
{
	printf("Successfully returned the block count\n");
	return blockCount;
}

int Level::getCheckpointCount(void)
{
	printf("Successfully returned the checkpoint count\n");
	return checkpointCount;
}

int Level::getSpikeCount(void)
{
	printf("Successfully returned the spike count\n");
	return spikeCount;
}

bool Level::readLevel(char* filename)
{
	//read the level in from a txt array and fill the level array with the contents
	FILE* f = fopen(filename,"r");

	char myChar;

	int i = 0;
	int j = 0;

	while((myChar = fgetc(f)) != EOF)
	{
		if(myChar == '\n')
		{
			i++;
			j = 0;
		}

		else
		{
			levelArray[i][j] = myChar;
			j++;
		}

		checkType(myChar);
	}

	printf("Successfully read in the entire level file\n");
	loadLevelFlag = true;
	fclose(f);
	return loadLevelFlag;
}

void Level::checkType(char c)
{
	if(c == 'b')
	{
		blockCount++;
	}

	if(c == 's')
	{
		spikeCount++;
	}

	if(c == 'c')
	{
		checkpointCount++;
	}
}

void Level::printLevel(void)
{
	for(int i = 0; i < 10; i++)
	{
		for(int j = 0; j < 30; j++)
		{
			printf("%c",levelArray[i][j]);
		}

		printf("\n");
	}

	printf("\n");
}

void Level::cleanUp(void)
{
	blockCount = 0;
	spikeCount = 0;
	checkpointCount = 0;
	
	for(int i = 0; i < 30; i++)
	{
		for(int j = 0; j < 30; j++)
		{
			levelArray[i][j] = 'x';
		}
	}

	printf("Level has been successfully cleaned up!\n");
}