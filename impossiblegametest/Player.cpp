#include "Player.h"


Player::Player(int x, int y)
{
	lives = 3;
	xPos = x;
	yPos = y;
	printf("A Player has been created at %d,%d \n",xPos,yPos );
	alive = true;
	printf("The player is alive\n");
	jumpTime =0;
	
}


Player::~Player(void)
{
	printf("A Player has been destroyed\n");
}

int Player::getX()
{
	printf("Returning a xPos value of %d\n",xPos);
	return xPos;
}
int Player::getY()
{
	printf("Returning a yPos value of %d\n",yPos);
	return yPos;
}

void Player::kill()
{
	alive = false;
	printf("The Player has been killed, one life lost\n");
	lives--;
}

void Player::Jump()
{
	jumping = true;
	printf("The player has jumped\n");
}

void Player::Update(TileType type)
{
	printf("Player is updating\n");
	if(jumping == true)
	{
		printf("Player is currently jumping, increase yPos and xPos\n");
		xPos++;
		yPos++;
		if(jumpTime ==4)
		{
			printf("Player has finished jumping, jumping set to false\n");
			jumping = false;
		}
		jumpTime++;
	}
	else
	{
		xPos++;
		printf("Player is not jumping, increase xPos and check if player can fall\n");
		if(type == EMPTY)
		{
			printf("Player can fall, decrease yPos\n");
			yPos--;
		}
		else
		{
			printf("Player cannot fall at present\n");
		}
	}
	printf("Player is now at %d,%d\n",xPos, yPos);
}