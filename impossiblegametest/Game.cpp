#include "Game.h"


Game::Game(HWND hwnd)
{
	hWnd = hwnd;

	backHDC = GetDC(hwnd);

	backBMP = NULL;
	oldBMP = NULL;

	// Back rectangle?


}


Game::~Game(void)
{
	
}


// Private functions

void Game::init(void)
{


}
	
void Game::cleanUp(void)
{
	// Needed here
	// delete level, player, game objects
}
	
void Game::swapBuffers(void)
{
	BitBlt(backHDC, 0, 0, windowWidth, windowHeight, backHdc, 0, 0, SRCCOPY); 
}

void Game::setupBackBuffer(void)
{
	backHdc = CreateCompatibleDC(globalHdc);
	backBmp = CreateCompatibleBitmap(globalHdc, windowWidth, windowHeight);
	oldBmp = (HBITMAP)SelectObject(backHdc, backBmp);
}



// Public functions

void Game::update(float)
{

}

void Game::collisionCheck(void)
{

}

bool Game::playerCheckpointCollision(RECT)
{
	return false;
}

bool Game::playerSpikeCollision(RECT)
{

	return false;
}

bool Game::playerBlockCollision(RECT)
{

	return true;
}

int Game::endLevel(void)
{
	// return level number - of last display scores etc

	return 2;
}

bool Game::handleKeyPress(WPARAM w)
{

	return false;
}

void Game::displaySplashScreen(void)
{

}

void Game::displayScore(void)
{

}


void Game::loadScores(void)
{

}

void Game::saveScores(void)
{


}
	
	
int Game::getLevelNum(void)
{


	return 1;
}

int Game::getPlayerLives(void)
{
	// lives = player->lives();

	return 2;
}

void Game::draw(void)
{
	// First draw background


	// Now other objects required


}