#pragma once

#include <Windows.h>

class Game
{
private:

	HWND hWnd;
	HDC backHDC;

	

	RECT scoreRT;
	RECT playerRT;
	RECT gameObjRT;


	HBITMAP backBMP;
	HBITMAP oldBMP;

	//-------------------------------------------------------------------------------
	// Change to documentation required maybe
	//
	// back rectangle for double buffering?
	//-------------------------------------------------------------------------------

	int score;
	bool levelCompleteFlag;
	int scrollSpeed;
	int scrollOffset;

	float scalar;
	char* filename;

	int levelFlag;

	bool hitFlag;

 
	// Instances / ptrs - To implement from other stubs

	/*Player* player;
	Level* level;
	Audio* audio;*/


	// Private functions

	void init(void);
	void cleanUp(void);
	
	void swapBuffers(void);
	void setupBackBuffer(void);


public:
	Game(void);
	~Game(void);

	
	void update(float);
	void collisionCheck(void);

	bool playerCheckpointCollision(RECT);
	bool playerSpikeCollision(RECT);
	bool playerBlockCollision(RECT);
	
	int endLevel(void);

	bool handleKeyPress(WPARAM w);
	void displaySplashScreen(void);
	void displayScore(void);
	void loadScores(void);
	void saveScores(void);
	
	
	int getLevelNum(void);
	int getPlayerLives(void);

	void draw(void);


};

